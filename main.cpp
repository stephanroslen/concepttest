#include <string_view>
#include <type_traits>

#include <fmt/core.h>

template<typename T, typename Sig>
struct InvocableAsHelper;

template<typename T, typename Res, typename... Args>
struct InvocableAsHelper<T, Res(Args...)> : public std::is_invocable_r<Res, T, Args...> {};

template<typename T, typename Res, typename... Args>
struct InvocableAsHelper<T, Res(Args...) const> : public std::is_invocable_r<Res, std::add_const_t<T>, Args...> {};

template<typename T, typename Res, typename... Args>
struct InvocableAsHelper<T, Res(Args...) noexcept> : public std::is_nothrow_invocable_r<Res, T, Args...> {};

template<typename T, typename Res, typename... Args>
struct InvocableAsHelper<T, Res(Args...) const noexcept>
    : public std::is_nothrow_invocable_r<Res, std::add_const_t<T>, Args...> {};

template<typename T, typename Sig>
concept InvocableAs = InvocableAsHelper<T, Sig>::value;

int main() {
  InvocableAs<size_t(std::string_view) const> auto v0 = [](const std::string_view v) {
    if (v.size() > 10) throw std::runtime_error{"This string is tooooooo big!"};
    return v.size();
  };
  InvocableAs<size_t(std::string_view)> auto v1 = [mem = size_t{0}](const std::string_view v) mutable {
    if (v.size() > 10) throw std::runtime_error{"This string is tooooooo big!"};
    return mem++ + v.size();
  };
  InvocableAs<size_t(std::string_view) const noexcept> auto v2 = [](const std::string_view v) noexcept {
    return v.size();
  };
  InvocableAs<size_t(std::string_view) noexcept> auto v3 =
      [mem = size_t{0}](const std::string_view v) mutable noexcept { return mem++ + v.size(); };

  constexpr std::string_view test{"test"};

  fmt::print("{} {} {} {}\n", v0(test), v1(test), v2(test), v3(test));
}